/**
 * 
 * 
 * @class Renderer
 */
class Renderer {

    /**
     * Creates an instance of Renderer.
     * 
     * @param {String} storageLocation - Name of storage location
     * @memberof Renderer
     */
    constructor(storageLocation) {
        window._uniqueRendererStorage = {};
        this.storageLocation = storageLocation;

        if (storageLocation && !window._uniqueRendererStorage[this.storageLocation]) {
            window._uniqueRendererStorage[this.storageLocation] = {};
        }
    }

    getRendered(id) {
        let data = window._uniqueRendererStorage[this.storageLocation][id];

        if (data) {
            return data.html;
        } else {
            let error = document.createElement('p');

            error.innerHTML = 'ERROR: Some error occurred while retrieving the data.';
            error.className = 'error';

            return error;
        }
    }

    /**
     * The function addRendered, appends the rendered HTML into the storage.
     * 
     * @param {String} renderId - ID for later retrieval
     * @param {HTMLElement} html - Rendered HTML element
     * @memberof Renderer
     */
    addRendered(renderId, html) {
        window._uniqueRendererStorage[this.storageLocation][renderId] = {
            html: html
        };
    }

    renderMeals(data) {
        let renderId = Date.now();
        console.log('renderMeals:', data);

        let elemMeals = document.createElement('div');

        if (data.meals) {
            for (let indexMeal = 0; indexMeal < data.meals.length; indexMeal++) {
                let meal = data.meals[indexMeal];

                if (meal) {
                    let elemMeal = document.createElement('section');
                    let elemTitle = document.createElement('h4');
                    let elemNotes = document.createElement('p');

                    elemTitle.innerHTML = meal.name;
                    elemNotes.innerHTML = meal.notes.join(', ');

                    elemMeal.appendChild(elemTitle);
                    elemMeal.appendChild(elemNotes);
                    elemMeals.appendChild(elemMeal);
                }
            }
        }

        this.addRendered(renderId, elemMeals);
        return '&lt;renderer ' + renderId + '&gt;';
    }

    renderMvv() {
        let renderId = Date.now();
        let elemMvv = document.createElement('iframe');

        let stationId = 1000053;    // Ottostraße
        // let stationId = 1000015;    // Karlstraße
        
        elemMvv.src = "http://efa.mvv-muenchen.de/xhr_departures?locationServerActive=1&stateless=1&type_dm=any&name_dm=" + stationId + "&useAllStops=1&useRealtime=1&limit=5&mode=direct&zope_command=enquiry%3Adepartures&compact=1&includedMeans=1&inclMOT_3=1&inclMOT_4=1&inclMOT_5=1&inclMOT_6=1&inclMOT_9=1&inclMOT_10=1&_=1508880137893"

        this.addRendered(renderId, elemMvv)
        return '&lt;renderer ' + renderId + '&gt;';
    }
}