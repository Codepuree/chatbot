let inMessage = document.getElementById('message-input');
let conMessages = document.getElementById('message-container');
let btnMic = document.getElementById('icon-mic');
let bot = new Bot();
let processedMessage = null;
let renderer = new Renderer('chatbot');
let recognition = new webkitSpeechRecognition();
recognition.lang = 'de-DE';

inMessage.addEventListener('keyup', onMessageInput);
bot.onProcesssingChange = onProcessingChange;

function onMessageInput(event) {
    event.preventDefault();
    if (event.keyCode == 13) {
        if (inMessage.value.length > 0) {
            addMessage(inMessage.value, 'me');
            // addMessage(bot.getAnswer(inMessage.value));
            bot.getAsyncAnswer(inMessage.value, addMessage);

            // Clear input
            inMessage.value = '';
        }
    }
}

/**
 * 
 * 
 * @param {String} messageText 
 * @param {String} user 
 */
function addMessage(messageText, user) {
    let messageElement = document.createElement('p');

    messageElement.className = 'message';

    if (user) {
        messageElement.classList.add('me');
    }

    let renderIds = messageText.match(/&lt;renderer\s[^&gt;]*&gt;/g);

    if (renderIds) {
        for (let indexId = 0; indexId < renderIds.length; indexId++) {
            let rawRenderId = renderIds[indexId];

            messageText = messageText.replace(rawRenderId, '');
        }
    }

    messageElement.innerHTML = messageText;

    if (!user) {
        let ssu = new SpeechSynthesisUtterance(messageText);
        ssu.lang = 'de';
        window.speechSynthesis.speak(ssu);
    }

    if (renderIds) {
        for (let indexId = 0; indexId < renderIds.length; indexId++) {
            let rawRenderId = renderIds[indexId];
            let renderId = rawRenderId.substring(13, rawRenderId.length - 4);

            messageElement.appendChild(renderer.getRendered(renderId));
        }
    }

    conMessages.appendChild(messageElement);
    messageElement.scrollIntoView();
}

function onProcessingChange(event) {
    console.log('Processing:', event);
    if (event.isProcessing) {
        let message = document.createElement('div');
        message.classList.add('message');
        message.classList.add('wave');

        for (let i = 0; i < 3; i++) {
            let dot = document.createElement('span');
            dot.className = 'dot';

            message.appendChild(dot);
        }

        conMessages.appendChild(message);
        processedMessage = message;
    } else {
        if (processedMessage) {
            processedMessage.parentElement.removeChild(processedMessage)
            processedMessage = null;
        }
    }
}

btnMic.addEventListener('click', event => {
    // recognition.start();
    alert('Coming soon...')
});