class Bot {
    constructor() {
        this.bot = new RiveScript();
        this.bot.loadFile("./bot/brain.rive", this.loadingDone.bind(this), this.loadingError.bind(this));
        this.username = 'local-user';
        this.renderer = new Renderer('chatbot');
        this._cache = {};

        this.bot.setSubroutine("checkForRain", (rs, args) => {
            return new rs.Promise((resolve, reject) => {
                let data = prompt('Enter a name:');

                if (data.length > 3) {
                    this.fireProcessChanged({ isProcessing: true });
                    $.getJSON("http://api.openweathermap.org/data/2.5/weather?q=London&units=metric&APPID=b644b2b32db864f7e669a832bbd0b350", function (json) {
                        JSON.stringify(json);
                        console.log('Weather API:', json);
                    });
                    setTimeout(_ => {
                        this.fireProcessChanged({ isProcessing: false });
                        resolve(data);
                    }, 5000);
                } else {
                    reject('Something went wrong!');
                }
            });
        });

        this.bot.setSubroutine("checkTemperatur", (rs, args) => {
            return new rs.Promise((resolve, reject) => {
                this.fireProcessChanged({ isProcessing: true });
                $.getJSON("http://api.openweathermap.org/data/2.5/weather?q=London&units=metric&APPID=b644b2b32db864f7e669a832bbd0b350",
                    (data) => {
                        this.fireProcessChanged({ isProcessing: false });
                        console.log('Weather API:', data);
                        resolve(data.main.temp + '° C');
                    },
                    (error) => {
                        reject('Something went wrong!');
                    });
            });
        });

        this.bot.setSubroutine("mensaOpenToday", (rs, args) => {
            return new rs.Promise((resolve, reject) => {
                this.fireProcessChanged({ isProcessing: true });
                $.getJSON("http://openmensa.org/api/v2/canteens/232/days",
                    (data) => {
                        this.fireProcessChanged({ isProcessing: false });
                        console.log('Mensa:', data);

                        let isClosed = false;
                        let currentDate = new Date().toISOString().substring(0, 10);

                        for (let indexDay = 0; indexDay < data.length; indexDay++) {
                            let day = data[indexDay];

                            if (day.date === currentDate) {
                                isClosed = day.closed;
                            }
                        }

                        resolve(isClosed ? 'closed' : 'open');
                    },
                    (error) => {
                        this.fireProcessChanged({ isProcessing: false });
                        reject('Something went wrong!');
                    })
            });
        });

        this.bot.setSubroutine("mensaMealsToday", (rs, args) => {
            return new rs.Promise((resolve, reject) => {
                this.fireProcessChanged({ isProcessing: true });
                $.getJSON("http://openmensa.org/api/v2/canteens/232/meals",
                    (data) => {
                        this.fireProcessChanged({ isProcessing: false });

                        let currentDate = new Date().toISOString().substring(0, 10);
                        let dayMeals = null;

                        for (let indexDay = 0; indexDay < data.length; indexDay++) {
                            let day = data[indexDay];

                            if (day.date === currentDate) {
                                dayMeals = day;
                            }
                        }

                        let renderedMealsId = this.renderer.renderMeals(dayMeals);
                        resolve(renderedMealsId);
                    },
                    (error) => {
                        this.fireProcessChanged({ isProcessing: false });
                        reject();
                    });
            });
        });


        this.bot.setSubroutine("mvvDepatures", (rs, args) => {
            console.log('args', args);
            return new rs.Promise((resolve, reject) => {
                $.ajax({
                    type: "GET",
                    url: "https://cors-anywhere.herokuapp.com/http://efa.mvv-muenchen.de/xhr_departures?locationServerActive=1&stateless=1&type_dm=any&name_dm=1000053&useAllStops=1&useRealtime=1&limit=5&mode=direct&zope_command=enquiry%3Adepartures&compact=1&includedMeans=1&inclMOT_3=1&inclMOT_4=1&inclMOT_5=1&inclMOT_6=1&inclMOT_9=1&inclMOT_10=1&_=1508880137893",
                    dataType: "text",
                    success: rawData => {
                        let parser = new DOMParser();
                        let domData = parser.parseFromString(rawData,"text/xml");
                        let jsonData = this.parseMvvDepatures(domData)
                        console.log('Received this data from mvv:', jsonData);
                    },
                    error: error => {
                        console.error('ERRor:', error);
                    }
                });
                resolve(this.renderer.renderMvv());
            });
        });

        this.bot.setSubroutine("responseTimes", (rs, args) => {
            return new rs.Promise((resolve, reject) => {

            });
        });
    }

    loadingDone(batchNum) {
        console.log("Batch #" + batchNum + " has finished loading!");

        // Now the replies must be sorted!
        this.bot.sortReplies();
    }

    getAnswer(message) {
        let answer = this.bot.reply(this.username, message);
        answer = answer.replace('\n', '<br>');
        return answer;
    }

    loadingError(error) {
        console.log("Error when loading files: " + error);
    }

    /**
     * The function getAsyncAnswer, returns the processed message to the callback.
     * 
     * @param {String} message 
     * @param {Function} callback 
     */
    getAsyncAnswer(message, callback) {
        this.bot.replyAsync(this.username, message).then(function (reply) {
            callback(reply);
        }, function (error) {
            console.error('ERROR:', error);
        });
    }

    /**
     * 
     * @param {Event} event 
     */
    fireProcessChanged(event) {
        if (typeof (this.onProcesssingChange) === 'function') {
            this.onProcesssingChange(event);
        }
    }

    /**
     * The function parses the received MVV depature data
     * 
     * @param {HTMLElement} rawData
     */
    parseMvvDepatures(rawData) {
        let tblDepatures = /** HTMLTableElement */ rawData.getElementsByTagName('table')[0];
        let tblhdrDepatures = tblDepatures.getElementsByTagName('thead')[0].getElementsByTagName('tr')[0].getElementsByTagName('th');
        let tblbdyDepatures = tblDepatures.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
        let retData = {
            header: [],
            body: []
        };

        for (let i = 0; i < tblhdrDepatures.length; i++) {
            let tblhd = tblhdrDepatures[i];
            retData.header.push(tblhd.innerHTML);
        }

        console.log('body:', tblbdyDepatures);
        for (let i = 0; i < tblbdyDepatures.length; i++) {
            let tblrw = tblbdyDepatures[i];
            let rwclls = tblrw.getElementsByTagName('td');
            let row = [];

            for (let j = 0; j < rwclls.length; j++) {
                let cll = rwclls[j];

                row.push(cll.innerHTML);
            }
        }

        return retData;
    }

    getResponseTimes(name) {
        
    }

    loadResponseTimes(onSuccess, onError) {
        $.getJSON('./data/response_times.json',
        data => {
            this._cache.responseTimes = data;
            onSuccess(data);
        },
        error => {
            onError(error);
        });
    }
}